# Scraping Emojipedia

- https://emojipedia.org/green-apple/ -> [scrape-emojipedia-page.js](scrape-emojipedia-page.js)
- https://emojipedia.org/apple/ -> [scrape-emojipedia-list.js](scrape-emojipedia-list.js)

- Pre-scraped Apple emoji URL list -> [apple.json](apple.json)
